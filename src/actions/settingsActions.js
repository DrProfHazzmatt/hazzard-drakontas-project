import * as types from './actionTypes';

export function saveSettings(json) 
{
    return {type: types.SAVE_SETTINGS, obj: json};
}
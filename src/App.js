import React, { Component } from 'react';
import TestComponent from './components/testComponent';
import './App.css';

class App extends Component 
{
	constructor(props)
	{
		super(props);

		this.state = {
			temperature: null,
			humidity: null
		}
	}

	geoLocate(evt) 
	{
		evt.preventDefault();
		
		navigator.geolocation.getCurrentPosition((position) => {

			let apiUrl = "https://api.openweathermap.org/data/2.5/weather?lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&units=metric&appid=785db87f255138bc116a3445c8bc16a1";
			let xhr = new XMLHttpRequest();

			xhr.open('GET', apiUrl, true);
			xhr.send();

			xhr.onload = () => {
				if(xhr.status >= 200 && xhr.status < 300)
				{
					let jsonObj = JSON.parse(xhr.response);

					this.setState({
						temperature: Math.round(jsonObj.main.temp) + "\xB0C",
						humidity: jsonObj.main.humidity + "%"
					});
				}
				else
				{
					console.log("Failure");
					console.log(xhr);
				}
			};
		});
	}
	
	render() 
	{
    	return (
      		<div className="App">
				<div className="info">
					<h1>Temp</h1>
					<div className="data">
						{this.state.temperature}
					</div>
				</div>
				<div className="info">
					<h1>Humidity</h1>
					<div className="data">
						{this.state.humidity}
					</div>
				</div>
        		<div>
					<button type="button" className="button" onClick={(evt) => this.geoLocate(evt)}>Geolocate</button>
				</div>
				<div className="footer">
					<TestComponent />
					<p>Source Material for this project can be found <a href="https://gitlab.com/DrProfHazzmatt/hazzard-drakontas-project">here</a>.</p>
				</div>
    		</div>
		);
	}
}

export default App;

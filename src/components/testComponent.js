import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as settingsActions from '../actions/settingsActions';
import React, { Component } from 'react';

class testComponent extends Component 
{
    constructor(props) {
        super(props);

        console.log('props for this component', props);
    }

    render() {
        return (
            <div>Useless stuff</div>
        )
    }
}

const mapStateToProps = state => {
    console.log(state);

    return { 
        settings: state.settings 
    };
}

const mapDispatchToProps = dispatch => {
    return {
        testActions: bindActionCreators(settingsActions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(testComponent);